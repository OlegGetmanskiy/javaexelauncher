#include <windows.h>
#include <iostream>
#include <cstring>
#include <stdio.h>
#ifdef WINDOWS
    #include <direct.h>
    #define GetCurrentDir _getcwd
#else
    #include <unistd.h>
    #define GetCurrentDir getcwd
#endif

using namespace std;

char cCurrentPath[FILENAME_MAX];

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	if (!GetCurrentDir(cCurrentPath, sizeof(cCurrentPath)))
   {
      return errno;
   }
   cCurrentPath[sizeof(cCurrentPath) - 1] = '\0';
   string exe(cCurrentPath);
   exe += "\\jre\\bin\\java.exe";
   
   ShellExecute(NULL, "open", exe.c_str(), " -jar light-objective.jar", NULL, SW_HIDE);
   
   return 0;
}
